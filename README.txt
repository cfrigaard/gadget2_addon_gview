This module views a snapshot file under OpenGL. See manual page for more information (run man ./gview.1).

Build executable by running a "make" command.
Prereqisites: C++ compiler (Gcc), STL, Glut open gl library

Version:    0.6.199
Date:       Dec 23, 2007
Site:       http://frigaard.homelinux.org/pub/gadget_addon_gview-0.6.tgz
Copyright:  Carsten Frigaard

Version 0.6:
	fix id sort bug
	made sync with Gadget-add source tighter
	added particle sorting
	added particle follow
	added snapshot debug output
	added griddrawing functionality
	improved keyboard shortcuts
	added mouse click move (disabled permament rotation mode)
	modified output message texts

The code is copyrighted 2007 by Carsten Frigaard.
All rights placed in public domain under GNU licence, 2007

© 2007 Carsten Frigaard. Permission to use, copy, modify, and distribute this software
and its documentation for any purpose and without fee is hereby granted, provided that
the above copyright notice appear in all copies and that both that copyright notice
and this permission notice appear in supporting documentation.

